

<footer class="bg1">
		<div class="container p-t-40 p-b-70">
			<div class="row">
				<div class="col-sm-6 col-md-4 p-t-50">
					<!-- - -->
					<h4 class="txt13 m-b-33">
						Contact Us
					</h4>

					<ul class="m-b-70">
						<li class="txt14 m-b-14">
							<i class="fa fa-map-marker fs-16 dis-inline-block size19" aria-hidden="true"></i>
							<?php echo $footerData ? $footerData->address : "";?>
						</li>

						<li class="txt14 m-b-14">
							<i class="fa fa-phone fs-16 dis-inline-block size19" aria-hidden="true"></i>
							<?php echo $footerData ? $footerData->phone : "";?>
						</li>

						<li class="txt14 m-b-14">
							<i class="fa fa-envelope fs-13 dis-inline-block size19" aria-hidden="true"></i>
							<?php echo $footerData ? $footerData->email : "";?>
						</li>
					</ul>

					
				</div>
				<div class="col-sm-6 col-md-4 p-t-50">
					<!-- - -->
					<h4 class="txt13 m-b-32">
						Opening Times
					</h4>

					<ul>
						<li class="txt14">
                        <?php echo $footerData ? date("h:i A", strtotime($footerData->opening_time)) : "";?> – <?php echo $footerData ? date("h:i A", strtotime($footerData->closing_time)) : "";?>
						</li>

						<li class="txt14">
                        <?php echo $footerData ? $footerData->opening_days : "";?>
						</li>
					</ul>
				</div>

			
				<div class="col-sm-6 col-md-4 p-t-50">
					<!-- - -->
					<h4 class="txt13 m-b-38">
						Gallery
					</h4>

					<!-- Gallery footer -->
					<div class="wrap-gallery-footer flex-w">
					<?php  
						$count = 0;
						foreach($galleryImages as $image){
							if($count >= 12){
								break;
							}
							
							?>
							<a class="item-gallery-sidebar wrap-pic-w" href="<?php echo $image->image_path; ?>" data-lightbox="gallery-footer">
								<img src="<?php echo $image->image_path; ?>" style="height: 100%;" alt="GALLERY">
							</a>

							
							<?php
							$count++;
							
						}
					?>
						

					</div>

				</div>
			</div>
		</div>

		<div class="end-footer bg2">
			<div class="container">
				<div class="flex-sb-m flex-w p-t-22 p-b-22">
					<div class="p-t-5 p-b-5">
						<a href="<?php echo $footerData ? $footerData->tripadvisor_url : "#";?>" class="fs-15 c-white"><i class="fa fa-tripadvisor" aria-hidden="true"></i></a>
						<a href="<?php echo $footerData ? $footerData->facebook_url : "#";?>" class="fs-15 c-white"><i class="fa fa-facebook m-l-18" aria-hidden="true"></i></a>
						<a href="<?php echo $footerData ? $footerData->twitter_url : "#";?>" class="fs-15 c-white"><i class="fa fa-instagram m-l-18" aria-hidden="true"></i></a>
					</div>

					<div class="txt17 p-r-20 p-t-5 p-b-5">
						Copyright &copy; 2018 All rights reserved  |  This template is made with <i class="fa fa-heart"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
					</div>
				</div>
			</div>
		</div>
	</footer>