<?php

	

	$footerData = getFooterData();

	if($footerData){
		$footerData = $footerData->data;
	}
	

?>


<header>
		<!-- Header desktop -->
		<div class="wrap-menu-header gradient1 trans-0-4">
			<div class="container h-full">
				<div class="wrap_header trans-0-3">
					<!-- Logo -->
					<div class="logo">
						<a href="index.php">
							<img src="images/icons/logo.png" alt="IMG-LOGO" data-logofixed="images/icons/logo2.png">
						</a>
					</div>

					<!-- Menu -->
					<div class="wrap_menu  p-l-0-xl">
						<nav class="menu">
							<ul class="main_menu">
								<li>
									<a href="index.php">Home</a>
								</li>

								<li>
									<a href="<?php echo explode('/', $_SERVER['REQUEST_URI'])[1] != "index.php" ? "index.php" : ""; ?>#events">Events</a>
								</li>
								
								<li>
									<a href="<?php echo explode('/', $_SERVER['REQUEST_URI'])[1] != "index.php" ? "index.php" : ""; ?>#reservation">Reservation</a>
								</li>
								<li>
									<a href="<?php echo explode('/', $_SERVER['REQUEST_URI'])[1] != "index.php" ? "index.php" : ""; ?>#customer-reviews">Customer Reviews</a>
								</li>
							
								
								<li>
									<a href="contact.php">Contact</a>
								</li>
							</ul>
						</nav>
					</div>

					<!-- Social -->
					<div class="social flex-w flex-l-m p-r-20">
						<a href="<?php echo $footerData ? $footerData->tripadvisor_url : "#";?>"><i class="fa fa-tripadvisor" aria-hidden="true"></i></a>
						<a href="<?php echo $footerData ? $footerData->facebook_url : "#";?>"><i class="fa fa-facebook m-l-21" aria-hidden="true"></i></a>
						<a href="<?php echo $footerData ? $footerData->twitter_url : "#";?>"><i class="fa fa-instagram m-l-21" aria-hidden="true"></i></a>
						<a href="./order.php"><i class="fa fa-shopping-cart m-l-21" aria-hidden="true"></i><span id="item-in-cart" style="opacity: 0; position: absolute; width: 10px; height: 10px; color: #ec1d25; background-color: #ec1d25; border-radius: 50%;"></span></a>

						<button class="btn-show-sidebar m-l-33 trans-0-4"></button>
					</div>
				</div>
			</div>
		</div>
	</header>