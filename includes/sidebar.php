<aside class="sidebar trans-0-4">
		<!-- Button Hide sidebar -->
		<button class="btn-hide-sidebar ti-close color0-hov trans-0-4"></button>

		<!-- - -->
		<ul class="menu-sidebar p-t-95 p-b-70">
			<li class="t-center m-b-13">
				<a href="index.php" class="txt19">Home</a>
			</li>

			<li class="t-center m-b-13">
				<a href="<?php echo explode('/', $_SERVER['REQUEST_URI'])[1] != "index.php" ? "index.php" : ""; ?>#events" class="txt19">Events</a>
			</li>
			
		
			<li class="t-center m-b-13">
				<a href="<?php echo explode('/', $_SERVER['REQUEST_URI'])[1] != "index.php" ? "index.php" : ""; ?>#customer-reviews" class="txt19">Customer Reviews</a>
			</li>
		

			<li class="t-center m-b-33">
				<a href="contact.php" class="txt19">Contact</a>
			</li>

			<li class="t-center">
				<!-- Button3 -->
				<a href="<?php echo explode('/', $_SERVER['REQUEST_URI'])[1] != "index.php" ? "index.php" : ""; ?>#reservation" class="btn3 flex-c-m size13 txt11 trans-0-4 m-l-r-auto">
					Reservation
				</a>
			</li>
		</ul>

		<!-- - -->
		<div class="gallery-sidebar t-center p-l-60 p-r-60 p-b-40">
			<!-- - -->
			<h4 class="txt20 m-b-33">
				Gallery
			</h4>

			<!-- Gallery -->
			<div class="wrap-gallery-sidebar flex-w">
			<?php  
				$count = 0;
				foreach($galleryImages as $image){
					if($count >= 9){
						break;
					}
					
					?>
					<a class="item-gallery-sidebar wrap-pic-w" href="<?php echo $image->image_path; ?>" data-lightbox="gallery-footer">
						<img src="<?php echo $image->image_path; ?>" style="height: 100%;" alt="GALLERY">
					</a>

					
					<?php
					$count++;
					
				}
			?>
				

			</div>
		</div>
	</aside>
