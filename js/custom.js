function getCookie() {
    const name = "itemIds=";
    const cDecoded = decodeURIComponent(document.cookie); //to be careful
    const cArr = cDecoded .split('; ');
    let res;
    cArr.forEach(val => {
        if (val.indexOf(name) === 0) res = val.substring(name.length);
    })
    return res;
}


const cookie = getCookie();
if(cookie){
    $("#item-in-cart").css("opacity", "1");
}