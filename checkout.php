<?php
	include("./api_calls.php");


    if(isset($_POST['action'])){
        if($_POST['action'] == "checkCode"){
            $code = checkCode($_POST['code']);
			
			echo json_encode($code);
			exit();
        }else if($_POST['action'] == "submitorderdata"){
			$data = array(
				"customer_name" => $_POST['customer_name'],
				"address" => $_POST['address'],
				"city" => $_POST['city'],
				"email" => $_POST['email'],
				"phone" => $_POST['phone'],
				"order_price" => $_POST['order_price'],
				"discount_price" => $_POST['discount_price'],
				"total_price" => $_POST['total_price'],
				"order_notes" => $_POST['order_notes'],
				"order_items" => $_POST['order_items'],
			);
			$order = createOrder($data);
			
			echo json_encode($order);
			exit();
		}
    }

	$items = array();

	if(isset($_COOKIE['itemIds'])){
		$cookie = (array)json_decode($_COOKIE['itemIds']);
		$tempItems = getItems($cookie);
		$items = (array)$tempItems->data;
	}

	$categoriesItems  = getCategoriesItems();
	if($categoriesItems){


		$categoriesItems = $categoriesItems->data;
	}

	$deliveryCharge  = getDeliveryChargeData();
	if($deliveryCharge){


		$deliveryCharge = $deliveryCharge->data;
	}

	
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Order</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.png"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/themify/themify-icons.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/slick/slick.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/lightbox2/css/lightbox.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->


</head>
<body class="animsition">

	<!-- Header -->
	<?php include('./includes/header.php'); ?>

	<!-- Sidebar -->
	<?php include('./includes/sidebar.php'); ?>


	


	<!-- Order section -->
	<section class="section-order bg1-pattern p-t-180 p-b-10">
		
		<?php
		if($items){
			$itemLen = sizeof($items);
			$count = 0;
			?>
			<table class="table table-responsive" style="width: 90%; margin: 0 auto;">
				<tr>
					<th>ID</th>
					<th>Image</th>
					<th>Name</th>
					<th>Price</th>
					<th>Quantity</th>
					<th>Subtotal</th>
				</tr>
			<?php
			foreach($items as $key => $value){
				
				if($count >= 5){
					break;
				}

				$quantity = (array)json_decode($_COOKIE['itemIds']);
				$quantity = $quantity[$value->id];
				?>
				<tr>
					
					<td id="itemid<?php echo $key+1; ?>">
						<input type="hidden" name="item<?php echo $key+1; ?>" id="item<?php echo $key+1; ?>" value="<?php echo $value->id; ?>"> 
						<?php echo $key+1; ?>
					</td>
					<td id="itemimage<?php echo $key+1; ?>"><img src="<?php echo strpos(";", $value->images) ? explode(';', $value->images)[0] : $value->images; ?>" alt="item-img" style="width: 50px; height: auto;"></td>
					<td id="itemname<?php echo $key+1; ?>"><?php echo $value->name; ?></td>
					<td id="itemprice<?php echo $key+1; ?>"><?php echo ceil($value->price); ?></td>
					<td id="itemquantity<?php echo $key+1; ?>"><?php echo $quantity; ?></td>
					<td id="itemsubtotal<?php echo $key+1; ?>"><?php echo ceil((float)$value->price * (float)$quantity); ?></td>
				</tr>

				<?php
				$count++;
			}
			?>
			</table>
            <div style="display: flex; flex-direction: row-reverse;">
                
                <button type="button" id="add-coupon" class="btn3 flex-c-m p-2 txt11 trans-0-4" ><i class="fa fa-check"></i></button>
                <input type="text" id="coupon-code" name="coupon-code">
            </div>
			<div style="height: 40px; width: 90%; margin: 0 auto;">
				<p style="float: right;"><span style="font-weight: bold;">Total</span> : <span id="total"></span></p>
			</div>
            <div style="height: 40px; width: 90%; margin: 0 auto;">
				<p style="float: right;"><span style="font-weight: bold;">Discount</span> : <span id="discount">0</span></p>
			</div>

			<div style="height: 40px; width: 90%; margin: 0 auto;">
				<p style="float: right;"><span style="font-weight: bold;">Delivery Charge</span> : <span id="delivery-charge"><?php echo $deliveryCharge ? ceil($deliveryCharge->delivery_charges) : 0; ?></span></p>
			</div>

            <div style="height: 40px; width: 90%; margin: 0 auto;">
				<p style="float: right;"><span style="font-weight: bold;">Grand Total</span> : <span id="grandtotal"></span></p>
			</div>
			
			<?php
		}
		
		?>

	</section>
    <section class="section-order bg1-pattern p-b-10">
        <div style="width: 90%; margin: 0 auto;">
            <button type="button" class="btn3 flex-c-m size13 txt11 trans-0-4" id="continue-checkout">Continue</button>    
        </div>
    </section>
   
    <section class="section-order bg1-pattern p-b-10" id="userdata" style="display:none;">
        <div style="width: 90%; margin: 0 auto;">
            <div>
                <input type="text" id="name" name="name" placeholder="Name" class="p-2 m-2">
            </div>
            <div>
                <input type="text" id="address" name="address" placeholder="Address" class="p-2 m-2">
            </div>
            <div>
                <input type="text" id="city" name="city" placeholder="City" class="p-2 m-2">
            </div>
            <div>
                <input type="email" id="email" name="email" placeholder="Email" class="p-2 m-2">
            </div>
            <div>
                <input type="text" id="phone" name="phone" placeholder="Number" class="p-2 m-2">
            </div>
            <div>
                <textarea name="notes" id="notes" placeholder="notes" cols="30" rows="10" class="p-2 m-2"></textarea>
            </div>
            
            <div>
                <button type="button" class="btn3 size13 txt11 trans-0-4" id="confirm-order">Confirm Order</button>    
            </div>

        </div>
    </section>
	


	<!-- Footer -->
	<?php include('./includes/footer.php'); ?>

	<!-- Back to top -->
	<div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
	</div>

	<!-- Container Selection1 -->
	<div id="dropDownSelect1"></div>

	<div class="modal" tabindex="-1" id="alertModal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<h5 id="alert-message"></h5>
				
			</div>
			
			</div>
		</div>
	</div>



<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/bootstrap/js/popper.js"></script>
	<script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/daterangepicker/moment.min.js"></script>
	<script type="text/javascript" src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/slick/slick.min.js"></script>
	<script type="text/javascript" src="js/slick-custom.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/parallax100/parallax100.js"></script>
	<script type="text/javascript">
        $('.parallax100').parallax100();
	</script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/lightbox2/js/lightbox.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>
    <script src="js/custom.js"></script>

	<script>
        const totalItems = parseInt("<?php echo sizeof($items); ?>");
		$(document).ready(function(){

            $("#add-coupon").on("click", function(){
                const code = $("#coupon-code").val();
				let message = "";
                if(code){
                    $.ajax({
                        url: "./checkout.php",
                        type: "POST",
                        data: {
                            action: "checkCode",
                            code: code
                        },
                        success: function(data){
                            const jsonData = JSON.parse(data);
                            if(jsonData['message'] === "Code doesn't exists"){
								message = "Coupon doesn't exists or it got expired";
                                
                            }else if(jsonData['message'] === "Code successful"){
                                const coupon = jsonData['data'];
                                
                                const discount = parseFloat(coupon['discount']);
                                const total = parseFloat($("#total").text());
                                if(coupon['type'] === 0){
                                    $("#discount").text(discount.toFixed(2) + " %");
                                    let grandTotal = (total * discount) / 100;
                                    grandTotal = total - grandTotal;
                                    $("#grandtotal").text(grandTotal);
                                }else{
                                    $("#discount").text(discount.toFixed(2));
                                    let grandTotal = total - discount;
                                    $("#grandtotal").text(grandTotal);
                                }
                            }else if(jsonData['message'] === "Required parameters are missing"){
                                message = "Coupon can't be added";
                            }

							if(message !== ""){
								showAlert(message);
							}
                            
                        }
                    })
                }

				
            });


            $("#continue-checkout").on("click", function(){
                $("#userdata").css("display", "block");
            });

            $("#confirm-order").on("click", function(){
                const name = $("#name").val();
                const address = $("#address").val();
                const city = $("#city").val();
                const email = $("#email").val();
                const phone = $("#phone").val();
                let order_price = 0;
                if($("#total").text() !== ""){
                    order_price = $("#total").text();
                }
                let discountPrice = 0;
                if($("#discount").text() !== "0"){
                    discountPrice = parseFloat($("#total").text()) - parseFloat($("#grandtotal").text());
                }
                
                const total = $("#grandtotal").text();
                const notes = $("#notes").val();
				let orderItems = {};
				for(let i=1; i<=totalItems; i++){
					orderItems[$("#item" + i).val()] = {
						total: $("#itemsubtotal" + i).text(),
						quantity: $("#itemquantity" + i).text()
					};
				}

				let message = "";

				$.ajax({
					url: "./checkout.php",
					type: "POST",
					data: {
						action: "submitorderdata",
						customer_name : name,
						address: address,
						city: city,
						email: email,
						phone: phone,
						order_price: order_price,
						discount_price: discountPrice,
						total_price: total,
						order_notes: notes,
						order_items: orderItems
					},
					success: function(data){
						const jsonData = JSON.parse(data);
						if(jsonData['code'] === 200){
							message = "Your order placed successfully.";
						
						}else{
							message = "We are having issue while placing your order.";
						}
						document.cookie = 'itemIds=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
						if(jsonData['code'] === 200){
							window.location.href = "./thankyou.php";
						}

						if(message !== ""){
							showAlert(message);
						}
					}
				});

			

            });
			
		});

		

		function calculateTotal(){
			
			let total = 0;
			for(let i=1; i<=totalItems; i++){
				
				total += parseFloat($("#itemsubtotal" + i).text());
			}
			const deliveryCharge = parseFloat($("#delivery-charge").text());
			total += deliveryCharge;
			$("#total").text(total);
            $("#grandtotal").text(total);
		}

		calculateTotal();

		function showAlert(message){
			$("#alert-message").text(message);
			$("#alertModal").modal('toggle');
		}

	</script>


</body>
</html>
