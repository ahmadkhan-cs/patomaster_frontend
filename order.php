<?php
	include("./api_calls.php");

	$items = array();

	if(isset($_COOKIE['itemIds'])){
		$cookie = (array)json_decode($_COOKIE['itemIds']);
		$tempItems = getItems($cookie);
		$items = (array)$tempItems->data;
	}

	$categoriesItems  = getCategoriesItems();
	if($categoriesItems){


		$categoriesItems = $categoriesItems->data;
	}


	$deliveryCharge  = getDeliveryChargeData();
	if($deliveryCharge){


		$deliveryCharge = $deliveryCharge->data;
	}

	
	
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Order</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.png"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/themify/themify-icons.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/slick/slick.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/lightbox2/css/lightbox.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->


</head>
<body class="animsition">

	<!-- Header -->
	<?php include('./includes/header.php'); ?>

	<!-- Sidebar -->
	<?php include('./includes/sidebar.php'); ?>


	


	<!-- Order section -->
	<section class="section-order bg1-pattern p-t-180 p-b-114">
		
		<?php
		if($items){
			$itemLen = sizeof($items);
			$count = 0;
			?>
			<table class="table table-responsive" style="width: 90%; margin: 0 auto;">
				<tr>
					<th>ID</th>
					<th>Image</th>
					<th>Name</th>
					<th>Price</th>
					<th>Quantity</th>
					<th>Subtotal</th>
					<th>Action</th>
				</tr>
			<?php
			foreach($items as $key => $value){
				
				if($count >= 5){
					break;
				}

				$quantity = (array)json_decode($_COOKIE['itemIds']);
				$quantity = $quantity[$value->id];
				?>
				<tr id="item-<?php echo $key+1; ?>">
					<td id="itemid<?php echo $key+1; ?>"><?php echo $key+1; ?></td>
					<td id="itemimage<?php echo $key+1; ?>"><img src="<?php echo strpos(";", $value->images) ? explode(';', $value->images)[0] : $value->images; ?>" alt="item-img" style="width: 50px; height: auto;"></td>
					<td id="itemname<?php echo $key+1; ?>"><?php echo $value->name; ?></td>
					<td id="itemprice<?php echo $key+1; ?>"><?php echo ceil($value->price); ?></td>
					<td id="itemquantity<?php echo $key+1; ?>">
						<div style="display: flex;">
							<button class="btn3 flex-c-m p-2 txt6 trans-0-4" id="itemsubtract<?php echo $key+1; ?>" onclick="decreaseQuantity(<?php echo $key+1; ?>, <?php echo $value->id; ?>)"><i class="fa fa-minus"></i></button>
							<input style="text-align: center; width: 70px;" type="number" value="<?php echo $quantity; ?>" readonly id="itemquantityvalue<?php echo $key+1; ?>">
							<button class="btn3 flex-c-m p-2 txt6 trans-0-4" id="itemadd<?php echo $key+1; ?>" onclick="increaseQuantity(<?php echo $key+1; ?>, <?php echo $value->id; ?>)"><i class="fa fa-plus"></i></button>
						</div>
					</td>
					<td id="itemsubtotal<?php echo $key+1; ?>"><?php echo ceil((float)$value->price * (float)$quantity); ?></td>
					<td><button class="btn3 flex-c-m p-2 txt6 trans-0-4" onclick="deleteItem(<?php echo $key+1; ?>)" style="background-color: #ff0000 !important;"><i class="fa fa-trash"></i></button></td>
				</tr>

				<?php
				$count++;
			}
			?>
			</table>
			<div style="height: 100px; width: 90%; margin: 0 auto;">
				<p style="float: right;"><span style="font-weight: bold;">Delivery Charge</span> : <span id="delivery-charge"><?php echo $deliveryCharge ? ceil($deliveryCharge->delivery_charges) : 0; ?></span></p>
			</div>
			<div style="height: 100px; width: 90%; margin: 0 auto;">
				<p style="float: right;"><span style="font-weight: bold;">Total</span> : <span id="total"></span></p>
			</div>
			<div style="height: 100px; width: 90%; margin: 0 auto; display: flex; flex-direction: row-reverse;">
				<button type="button" class="btn3 flex-c-m size13 txt11 trans-0-4" ><a href="./checkout.php" style="color: #ffffff; text-decoration: none;">Checkout</a></button>
			</div>
			<?php
		}else{
			?>
			<div style="display: flex; justify-content: center">
				<p>Add something delicious to cart</p>
				
			</div>
			<div class="m-t-10" style="display: flex; justify-content: center">
				<button type="button" class="btn3 flex-c-m size13 txt11 trans-0-4" ><a href="./index.php" style="color: #ffffff; text-decoration: none;">View Menu</a></button>
			</div>
			<?php
		}
		
		?>

	</section>
	


	<!-- Footer -->
	<?php include('./includes/footer.php'); ?>

	<!-- Back to top -->
	<div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
	</div>

	<!-- Container Selection1 -->
	<div id="dropDownSelect1"></div>



<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/bootstrap/js/popper.js"></script>
	<script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/daterangepicker/moment.min.js"></script>
	<script type="text/javascript" src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/slick/slick.min.js"></script>
	<script type="text/javascript" src="js/slick-custom.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/parallax100/parallax100.js"></script>
	<script type="text/javascript">
        $('.parallax100').parallax100();
	</script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/lightbox2/js/lightbox.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>
    <script src="js/custom.js"></script>

	<script>
		$(document).ready(function(){
			
		});

		function decreaseQuantity(item, cookieItem){
			let itemValue = parseInt($("#itemquantityvalue" + item).val());
			if(itemValue > 1){
				itemValue--;
				const price = parseFloat($("#itemprice" + item).text());
			
				$("#itemquantityvalue" + item).val(itemValue);
				const subtotal = price * itemValue;

				$("#itemsubtotal" + item).text(subtotal);
				deleteFromCart(cookieItem);
				calculateTotal();
			}
			
		}

		function increaseQuantity(item, cookieItem){
			let itemValue = parseInt($("#itemquantityvalue" + item).val());
			itemValue++;
			const price = parseFloat($("#itemprice" + item).text());
			
			$("#itemquantityvalue" + item).val(itemValue);
			const subtotal = price * itemValue;

			$("#itemsubtotal" + item).text(subtotal);
			addToCart(cookieItem);
			calculateTotal();
		}


		function addToCart(itemId){
			const cookie = getCookie();
			let itemIds = {};
			
			itemIds = JSON.parse(cookie);
			itemIds[itemId]++;
			

			
			
			let date = new Date();
			date.setTime(date.getTime() + (24 * 60 * 60 * 1000));
			const expires = "expires=" + date.toUTCString();
			document.cookie = "itemIds=" + JSON.stringify(itemIds) + "; " + expires + "; path=/";

			

		}

		function deleteFromCart(itemId){
			const cookie = getCookie();
			let itemIds = {};
			
			
			itemIds = JSON.parse(cookie);
			itemIds[itemId]--;
			

			
			
			let date = new Date();
			date.setTime(date.getTime() + (24 * 60 * 60 * 1000));
			const expires = "expires=" + date.toUTCString();
			document.cookie = "itemIds=" + JSON.stringify(itemIds) + "; " + expires + "; path=/";

		}

		function calculateTotal(){
			const totalItems = parseInt("<?php echo sizeof($items); ?>");
			const deliveryCharge = parseFloat($("#delivery-charge").text());
			let total = 0;
			for(let i=1; i<=totalItems; i++){
				
				total += parseFloat($("#itemsubtotal" + i).text());
			}

			total += deliveryCharge;

			$("#total").text(total);
		}

		calculateTotal();


		function deleteItem(itemid){
			$("#item-" + itemid).remove();
			const cookie = getCookie();
			let itemIds = {};
			
			
			itemIds = JSON.parse(cookie);

			delete itemIds[itemid];
			if(Object.keys(itemIds).length === 0){
				window.location.href = "./index.php";
			}
			

			
			
			let date = new Date();
			date.setTime(date.getTime() + (24 * 60 * 60 * 1000));
			const expires = "expires=" + date.toUTCString();
			document.cookie = "itemIds=" + JSON.stringify(itemIds) + "; " + expires + "; path=/";

		}

	</script>


</body>
</html>
