<?php


function sendGetRequest($endpoint){
    $base_url = "http://127.0.0.1:8000/api";
    $cURLConnection = curl_init();
    curl_setopt($cURLConnection, CURLOPT_URL, $base_url . $endpoint);
    curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
    
    $response = curl_exec($cURLConnection);
    curl_close($cURLConnection);
    return json_decode($response);
}

function sendPostRequest($endpoint, $data){
    $base_url = "http://127.0.0.1:8000/api";
    $cURLConnection = curl_init();
    curl_setopt($cURLConnection, CURLOPT_URL, $base_url . $endpoint);
   
    $payload = json_encode($data);
    curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, $payload);
    curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
    
    $response = curl_exec($cURLConnection);
    
    curl_close($cURLConnection);
    
    return json_decode($response);
}

function getFooterData(){
    $response = sendGetRequest("/get-footer-data");
    return $response;
}


function getWelcomeImageData(){
    $response = sendGetRequest("/get-welcome-image-data");
    return $response;
}



function getDeliveryChargeData(){
    $response = sendGetRequest("/get-delivery-charge");
    return $response;
}




function getSliderData(){
    $response = sendGetRequest("/get-sliders-data");
    return $response;
}

function setNewsletter($email){
    $data = array("email" => $email);
    $response = sendPostRequest("/set-newsletter", $data);
    return $response;
}

function sendMessage($message){
    $data = array("name" => $message['name'], "email" => $message['email'], "phone" => $message['phone'], "message" => $message['message']);
    
    $response = sendPostRequest("/contact-us", $data);
    return $response;
}


function setReservation($reservationData){
    
    $response = sendPostRequest("/set-reservation", $reservationData);
    
    return $response;
}


function getItems($itemIds){
    $data = array("itemIds" => $itemIds);
    $response = sendPostRequest("/get-items", $data);
    return $response;
}


function getCategoriesItems(){
    $response = sendGetRequest("/get-categories-items");
    return $response;
}
function getGalleryImages(){
    $response = sendGetRequest("/get-gallery-images");
    return $response;
}

function getEvents(){
    $response = sendGetRequest("/get-events");
    return $response;
}


function getTestimonials(){
    $response = sendGetRequest("/get-testimonial-data");
    return $response;
}


function checkCode($code){
    $data = array("code" => $code);
    $response = sendPostRequest("/check-code", $data);
    return $response;
}


function createOrder($orderData){
    
    $response = sendPostRequest("/create-order", $orderData);
    return $response;
}

?>