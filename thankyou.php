<?php
	include("./api_calls.php");

	$items = array();

	if(isset($_COOKIE['itemIds'])){
		$cookie = (array)json_decode($_COOKIE['itemIds']);
		$tempItems = getItems($cookie);
		$items = (array)$tempItems->data;
	}

	$categoriesItems  = getCategoriesItems();
	if($categoriesItems){


		$categoriesItems = $categoriesItems->data;
	}

	
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Thank you</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.png"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/themify/themify-icons.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/slick/slick.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/lightbox2/css/lightbox.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->


</head>
<body class="animsition">

	<!-- Header -->
	<?php include('./includes/header.php'); ?>

	<!-- Sidebar -->
	<?php include('./includes/sidebar.php'); ?>


	


	<!-- Order section -->
	<section class="section-thankyou bg1-pattern p-t-180 p-b-114">
		
            <div style="display: flex; justify-content: center">
				<p>Thanks for placing order.</p>
				
			</div>
			<div class="m-t-10" style="display: flex; justify-content: center">
				<button type="button" class="btn3 flex-c-m size13 txt11 trans-0-4" ><a href="./index.php" style="color: #ffffff; text-decoration: none;">View Menu</a></button>
			</div>

	</section>
	


	<!-- Footer -->
	<?php include('./includes/footer.php'); ?>

	<!-- Back to top -->
	<div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
	</div>

	<!-- Container Selection1 -->
	<div id="dropDownSelect1"></div>



<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/bootstrap/js/popper.js"></script>
	<script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/daterangepicker/moment.min.js"></script>
	<script type="text/javascript" src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/slick/slick.min.js"></script>
	<script type="text/javascript" src="js/slick-custom.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/parallax100/parallax100.js"></script>
	<script type="text/javascript">
        $('.parallax100').parallax100();
	</script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/lightbox2/js/lightbox.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>
    <script src="js/custom.js"></script>

	<script>
		$(document).ready(function(){
			
		});

		function decreaseQuantity(item, cookieItem){
			let itemValue = parseInt($("#itemquantityvalue" + item).val());
			if(itemValue > 1){
				itemValue--;
				const price = parseFloat($("#itemprice" + item).text());
			
				$("#itemquantityvalue" + item).val(itemValue);
				const subtotal = price * itemValue;

				$("#itemsubtotal" + item).text(subtotal);
				deleteFromCart(cookieItem);
				calculateTotal();
			}
			
		}

		function increaseQuantity(item, cookieItem){
			let itemValue = parseInt($("#itemquantityvalue" + item).val());
			itemValue++;
			const price = parseFloat($("#itemprice" + item).text());
			
			$("#itemquantityvalue" + item).val(itemValue);
			const subtotal = price * itemValue;

			$("#itemsubtotal" + item).text(subtotal);
			addToCart(cookieItem);
			calculateTotal();
		}


		function addToCart(itemId){
			const cookie = getCookie();
			let itemIds = {};
			
			itemIds = JSON.parse(cookie);
			itemIds[itemId]++;
			

			
			
			let date = new Date();
			date.setTime(date.getTime() + (24 * 60 * 60 * 1000));
			const expires = "expires=" + date.toUTCString();
			document.cookie = "itemIds=" + JSON.stringify(itemIds) + "; " + expires + "; path=/";

			

		}

		function deleteFromCart(itemId){
			const cookie = getCookie();
			let itemIds = {};
			
			
			itemIds = JSON.parse(cookie);
			itemIds[itemId]--;
			

			
			
			let date = new Date();
			date.setTime(date.getTime() + (24 * 60 * 60 * 1000));
			const expires = "expires=" + date.toUTCString();
			document.cookie = "itemIds=" + JSON.stringify(itemIds) + "; " + expires + "; path=/";

		}

		function calculateTotal(){
			const totalItems = parseInt("<?php echo sizeof($items); ?>");
			let total = 0;
			for(let i=1; i<=totalItems; i++){
				
				total += parseFloat($("#itemsubtotal" + i).text());
			}

			$("#total").text(total);
		}

		calculateTotal();

	</script>


</body>
</html>
