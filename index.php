<?php
	include("./api_calls.php");

	if(isset($_POST['action'])){
		if($_POST['action'] == "newsletter"){
			$newsletter = setNewsletter($_POST['email']);
			
			echo json_encode($newsletter);
			exit();
		}else if($_POST['action'] == "reservation"){
			
			$data = array(
				"date" => $_POST['date'],
				"time" => $_POST['time'],
				"people" => $_POST['people'],
				"name" => $_POST['name'],
				"phone" => $_POST['phone'],
				"email" => $_POST['email'],
			);


			$reservation = setReservation($data);
			
			echo json_encode($reservation);
			exit();
		}
	}


	
	$slidersData  = getSliderData();
	if($slidersData){


		$slidersData = $slidersData->data;
	}



	$welcomeImage  = getWelcomeImageData();
	if($welcomeImage){


		$welcomeImage = $welcomeImage->data;
	}


	$categoriesItems  = getCategoriesItems();
	if($categoriesItems){


		$categoriesItems = $categoriesItems->data;
	}

	$galleryImages = getGalleryImages();
	if($galleryImages){


		$galleryImages = $galleryImages->data;
	}

	$eventsData = getEvents();
	if($eventsData){


		$eventsData = $eventsData->data;
	}

	$testimonialsData = getTestimonials();
	if($testimonialsData){


		$testimonialsData = $testimonialsData->data;
	}
	


?>


<!DOCTYPE html>
<html lang="en">
<head>
	<title>Home</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.png"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/themify/themify-icons.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/slick/slick.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/lightbox2/css/lightbox.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->


<style>
	.item-add:hover{
		background-color: #760f13;
		cursor: pointer;
	}
</style>
</head>
<body class="animsition">

	<!-- Header -->
	<?php include('./includes/header.php'); ?>

	<!-- Sidebar -->
	<?php include('./includes/sidebar.php'); ?>
	
	<!-- Slide1 -->
	<section class="section-slide">
		<div class="wrap-slick1">
			<div class="slick1">
				<?php 
				if($slidersData){
					foreach($slidersData as $slider){
						?>
						<div class="item-slick1 item1-slick1" style="background-image: url(<?php echo $slider->image; ?>);">
							<div class="wrap-content-slide1 sizefull flex-col-c-m p-l-15 p-r-15 p-t-150 p-b-170">
								<span class="caption1-slide1 txt1 t-center animated visible-false m-b-15" data-appear="fadeInDown">
									Welcome to
								</span>

								<h2 class="caption2-slide1 tit1 t-center animated visible-false m-b-37" data-appear="fadeInUp">
									<?php echo $welcomeImage? $welcomeImage->restaurant_name : ""; ?>
								</h2>

								<div class="wrap-btn-slide1 animated visible-false" data-appear="zoomIn">
									<!-- Button1 -->
									<a href="#section-menu-reference" class="btn1 flex-c-m size1 txt3 trans-0-4">
										Look Menu
									</a>
								</div>
							</div>
						</div>
						<?php
					}
				}
				?>
				

			

			</div>

			<div class="wrap-slick1-dots"></div>
		</div>
	</section>

	<!-- Welcome -->
	<section class="section-welcome bg1-pattern p-t-120 p-b-105">
		<div class="container">
			<div class="row">
				<div class="col-md-6 p-t-45 p-b-30">
					<div class="wrap-text-welcome t-center">
						<span class="tit2 t-center">
						<?php echo $welcomeImage? $welcomeImage->title : ""; ?>
						</span>

						<h3 class="tit3 t-center m-b-35 m-t-5">
						<?php echo $welcomeImage? $welcomeImage->message : ""; ?>
						</h3>

						<p class="t-center m-b-22 size3 m-l-r-auto">
						<?php echo $welcomeImage? $welcomeImage->description : ""; ?>
						</p>

						
					</div>
				</div>

				<div class="col-md-6 p-b-30">
					<div class="wrap-pic-welcome size2 bo-rad-10 hov-img-zoom m-l-r-auto">
						
						<img src="<?php echo $welcomeImage ? $welcomeImage->image: ''; ?>" alt="IMG-OUR">
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="section-menu-reference">

	</section>
	<?php
	if($categoriesItems){
		foreach($categoriesItems as $ci){
			if(sizeof($ci->items) > 0){
			?>
				<section class="section-lunch bgwhite " >
					<div class="header-lunch parallax0 parallax100" style="background-image: url(<?php echo $ci->category->img_url; ?>);">
						<div class="bg1-overlay t-center p-t-170 p-b-165">
							<h2 class="tit4 t-center">
								<?php echo $ci->category->name; ?>
							</h2>
						</div>
					</div>

					<div class="container">
						<div class="row p-t-108 p-b-70">
							<?php
							foreach($ci->items as $item){
								?>
								<div class="col-md-8 col-lg-6 m-l-r-auto">
									<!-- Block3 -->
									<div class="blo3 flex-w flex-col-l-sm m-b-30">
										<div class="pic-blo3 size20 bo-rad-10 hov-img-zoom m-r-28">
											<a href="#" onclick='showModal(event, <?php echo json_encode($item); ?>);'><img src="<?php echo $item->images; ?>" style="height: 100%" alt="IMG-MENU"></a>
										</div>

										<div class="text-blo3 size21 flex-col-l-m">
											<a href="#" class="txt21 m-b-2">
												<?php echo $item->name; ?>
											</a>

											<span class="txt23">
												<?php echo substr($item->description, 0, 100) . "..."; ?>
											</span>

											<span class="txt22 m-t-10" style="display: flex;">
												
												<span class="m-r-20"><?php echo ceil($item->price); ?> PKR</span>
												<?php if($item->sold_out == 0){ ?>
												
												<span style="background-color: #ec1d25; color: #ffffff; border-radius: 50%; width: 25px; height: 25px; font-size: 16px; display: flex; justify-content: center; align-items: center;" class="item-add m-r-20" onclick="addToCart('<?php echo $item->id; ?>');">
													<i class="fa fa-cart-plus"></i> 
												</span>
												<span style="color: #ec1d25; opacity: 0;" class="txt20" id="item-added-<?php echo $item->id; ?>">Added to cart</span>
												
												<?php }?>
											</span>
											
										</div>
										<div class="mt-1">
											<?php
											if($item->special_item == 1){
												?>
												<span class="badge badge-danger mx-1">Special Item</span>
												<?php
											}
											
											if($item->sold_out == 1){
												?>
												<span class="badge badge-danger mx-1">Sold Out</span>
												<?php
											}

											if($item->top_pick_today == 1){
												?>
												<span class="badge badge-danger mx-1">Top pick of the day</span>
												<?php
											}
											?>
										</div>
										
									</div>
								</div>	

								<?php
							}
							
							?>

							<?php
							if(sizeof($ci->items) % 2 == 1){
								?>
								<div class="col-md-8 col-lg-6 m-l-r-auto">
								</div>
								<?php
							}
							?>
							
						</div>
					</div>
				</section>

			<?php
			}
		}
	}
	?>

	<?php if($eventsData){ ?>
		<!-- Event -->
		<section class="section-event" id="events">
			<div class="wrap-slick2">
				<div class="slick2">
					<?php
					
						$count = 1;
						foreach($eventsData as $event){
							?>		
								<div class="item-slick2 item1-slick2" style="background-image: url(images/bg-event-01.jpg);">
									<div class="wrap-content-slide2 p-t-115 p-b-208">
										<div class="container"  id="event-<?php echo $count; ?>">
											<!-- - -->
											<div class="title-event t-center m-b-52">
												<span class="tit2 p-l-15 p-r-15">
													Upcomming
												</span>

												<h3 class="tit6 t-center p-l-15 p-r-15 p-t-3">
													Events
												</h3>
											</div>

											<!-- Block2 -->
											<div class="blo2 flex-w flex-str flex-col-c-m-lg animated visible-false" data-appear="zoomIn">
												<!-- Pic block2 -->
												<a href="#" class="wrap-pic-blo2 bg1-blo2" style="background-image: url(<?php echo $event->event_image ?>);">
													<div class="time-event size10 txt6 effect1">
														<span class="txt-effect1 flex-c-m t-center">
															<?php echo date("h:i A D - d M Y", strtotime($event->event_end)); ?>
															
														</span>
													</div>
												</a>

												<!-- Text block2 -->
												<div class="wrap-text-blo2 flex-col-c-m p-l-40 p-r-40 p-t-45 p-b-30">
													<h4 class="tit7 t-center m-b-10">
														<?php echo $event->event_title; ?>
													</h4>

													<p class="t-center">
													<?php echo $event->event_description; ?>
													</p>
													<input type="hidden" name="end_date<?php echo $count; ?>" id="end_date<?php echo $count; ?>" value="<?php echo date("Y-m-d H:i:s", strtotime("+5 hours", strtotime($event->event_end))); ?>">

													<?php

													$datetime1 = new DateTime(date("Y-m-d H:i:s", strtotime("+5 hours", strtotime($event->event_end))));
													$datetime2 = new DateTime(date('Y-m-d H:i:s'));
													$interval = $datetime1->diff($datetime2);
													
													
													
													?>

													<div class="flex-sa-m flex-w w-full m-t-40">
														<div class="size11 flex-col-c-m">
															<span class="dis-block t-center txt7 m-b-2" id="days<?php echo $count; ?>">
																<?php echo $interval->format('%d'); ?>
															</span>

															<span class="dis-block t-center txt8">
																Days
															</span>
														</div>

														<div class="size11 flex-col-c-m">
															<span class="dis-block t-center txt7 m-b-2" id="hours<?php echo $count; ?>">
															<?php echo $interval->format('%h'); ?>
															</span>

															<span class="dis-block t-center txt8">
																Hours
															</span>
														</div>

														<div class="size11 flex-col-c-m">
															<span class="dis-block t-center txt7 m-b-2" id="minutes<?php echo $count; ?>">
															<?php echo $interval->format('%i'); ?>
															</span>

															<span class="dis-block t-center txt8">
																Minutes
															</span>
														</div>

														<div class="size11 flex-col-c-m">
															<span class="dis-block t-center txt7 m-b-2" id="seconds<?php echo $count; ?>">
															<?php echo $interval->format('%s'); ?>
															</span>

															<span class="dis-block t-center txt8">
																Seconds
															</span>
														</div>
													</div>

												
												</div>
											</div>
										</div>
									</div>
								</div>
							<?php
							$count++;
						}
					
					?>
					

				</div>

				<div class="wrap-slick2-dots"></div>
			</div>
		</section>
	<?php } ?>

	<!-- Booking -->
	<section class="section-booking bg1-pattern p-t-100 p-b-110" id="reservation">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 p-b-30">
					<div class="t-center">
						<span class="tit2 t-center">
							Reservation
						</span>

						<h3 class="tit3 t-center m-b-35 m-t-2">
							Book table
						</h3>
					</div>

					<form class="wrap-form-booking" name="reservation-form">
						<div class="row">
							<div class="col-md-6">
								<!-- Date -->
								<span class="txt9">
									Date
								</span>

								<div class="wrap-inputdate pos-relative txt10 size12 bo2 bo-rad-10 m-t-3 m-b-23">
									<input class="my-calendar bo-rad-10 sizefull txt10 p-l-20" type="text" name="date">
									<i class="btn-calendar fa fa-calendar ab-r-m hov-pointer m-r-18" aria-hidden="true"></i>
								</div>

								<!-- Time -->
								<span class="txt9">
									Time
								</span>

								<div class="wrap-inputtime size12 bo2 bo-rad-10 m-t-3 m-b-23">
									<!-- Select2 -->
									<select class="selection-1" name="time">
										<option>9:00</option>
										<option>9:30</option>
										<option>10:00</option>
										<option>10:30</option>
										<option>11:00</option>
										<option>11:30</option>
										<option>12:00</option>
										<option>12:30</option>
										<option>13:00</option>
										<option>13:30</option>
										<option>14:00</option>
										<option>14:30</option>
										<option>15:00</option>
										<option>15:30</option>
										<option>16:00</option>
										<option>16:30</option>
										<option>17:00</option>
										<option>17:30</option>
										<option>18:00</option>
									</select>
								</div>

								<!-- People -->
								<span class="txt9">
									People
								</span>

								<div class="wrap-inputpeople size12 bo2 bo-rad-10 m-t-3 m-b-23">
									<!-- Select2 -->
									<select class="selection-1" name="people">
										<option value="1">1 person</option>
										<option value="2">2 people</option>
										<option value="3">3 people</option>
										<option value="4">4 people</option>
										<option value="5">5 people</option>
										<option value="6">6 people</option>
										<option value="7">7 people</option>
										<option value="8">8 people</option>
										<option value="9">9 people</option>
										<option value="10">10 people</option>
										<option value="11">11 people</option>
										<option value="12">12 people</option>
									</select>
								</div>
							</div>

							<div class="col-md-6">
								<!-- Name -->
								<span class="txt9">
									Name
								</span>

								<div class="wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
									<input class="bo-rad-10 sizefull txt10 p-l-20" type="text" name="name" placeholder="Name">
								</div>

								<!-- Phone -->
								<span class="txt9">
									Phone
								</span>

								<div class="wrap-inputphone size12 bo2 bo-rad-10 m-t-3 m-b-23">
									<input class="bo-rad-10 sizefull txt10 p-l-20" type="text" name="phone" placeholder="Phone">
								</div>

								<!-- Email -->
								<span class="txt9">
									Email
								</span>

								<div class="wrap-inputemail size12 bo2 bo-rad-10 m-t-3 m-b-23">
									<input class="bo-rad-10 sizefull txt10 p-l-20" type="text" name="email" placeholder="Email">
								</div>
							</div>
						</div>

						<div class="wrap-btn-booking flex-c-m m-t-6">
							<!-- Button3 -->
							<button type="button" class="btn3 flex-c-m size13 txt11 trans-0-4" id="make-reservation">
								Book Table
							</button>
						</div>
					</form>
				</div>

				<div class="col-lg-6 p-b-30 p-t-18">
					<div class="wrap-pic-booking size2 bo-rad-10 hov-img-zoom m-l-r-auto">
						<img src="images/booking-01.jpg" alt="IMG-OUR">
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Review -->
	<section class="section-review p-t-115" id="customer-reviews">
		<!-- - -->
		<div class="title-review t-center m-b-2">
			<span class="tit2 p-l-15 p-r-15">
				Customers Say
			</span>

			<h3 class="tit8 t-center p-l-20 p-r-15 p-t-3">
				Review
			</h3>
		</div>

		<!-- - -->
		<div class="wrap-slick3">
			<div class="slick3">
				<?php
				if($testimonialsData){
					foreach($testimonialsData as $testimonial){
						?>
							<div class="item-slick3 item1-slick3">
								<div class="wrap-content-slide3 p-b-50 p-t-50">
									<div class="container">
										<div class="pic-review size14 bo4 wrap-cir-pic m-l-r-auto animated visible-false" data-appear="zoomIn">
											<img src="<?php echo $testimonial->client_image; ?>" alt="IGM-AVATAR">
										</div>

										<div class="content-review m-t-33 animated visible-false" data-appear="fadeInUp">
											<p class="t-center txt12 size15 m-l-r-auto">
												“ <?php echo $testimonial->client_message; ?> ”
											</p>

										

											<div class="more-review txt4 t-center animated visible-false m-t-32" data-appear="fadeInUp">
												<?php echo $testimonial->client_name; ?> ˗ <?php echo $testimonial->client_designation; ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php
					}
				}
				?>
			


			</div>

			<div class="wrap-slick3-dots m-t-30"></div>
		</div>
	</section>


	

	


	<!-- Sign up -->
	<div class="section-signup bg1-pattern p-t-85 p-b-85">
		<form class="flex-c-m flex-w flex-col-c-m-lg p-l-5 p-r-5">
			<span class="txt5 m-10">
				Specials Sign up
			</span>

			<div class="wrap-input-signup size17 bo2 bo-rad-10 bgwhite pos-relative txt10 m-10">
				<input class="bo-rad-10 sizefull txt10 p-l-20" type="text" name="email-address" id="email-address" placeholder="Email Adrress">
				<i class="fa fa-envelope ab-r-m m-r-18" aria-hidden="true"></i>
			</div>

			<!-- Button3 -->
			<button type="button" class="btn3 flex-c-m size18 txt11 trans-0-4 m-10" id="newsletter">
				Sign-up
			</button>
		</form>
	</div>


	<!-- Footer -->
	<?php include('./includes/footer.php'); ?>


	<!-- Back to top -->
	<div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
	</div>

	<!-- Container Selection1 -->
	<div id="dropDownSelect1"></div>

	<!-- Modal Video 01-->
	<div class="modal fade" id="modal-video-01" tabindex="-1" role="dialog" aria-hidden="true">

		<div class="modal-dialog" role="document" data-dismiss="modal">
			<div class="close-mo-video-01 trans-0-4" data-dismiss="modal" aria-label="Close">&times;</div>

			<div class="wrap-video-mo-01">
				<div class="w-full wrap-pic-w op-0-0"><img src="images/icons/video-16-9.jpg" alt="IMG"></div>
				<div class="video-mo-01">
					<iframe src="https://www.youtube.com/embed/5k1hSu2gdKE?rel=0&amp;showinfo=0" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>

	<div class="modal" tabindex="-1" id="itemModal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Item</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<img id="item-img" src="" alt="item-img" style="width: 100%; height: auto;">
					</div>
					<div class="col-md-6">
						<p id="item-name" class="txt21 m-b-2"></p>
						<p id="item-desc" class="txt23"></p>
						<p id="item-price" class="txt22 m-t-20"></p>
						<p id="item-tags"></p>
					</div>
				</div>
				
			</div>
			
			</div>
		</div>
	</div>
	<div class="modal" tabindex="-1" id="alertModal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<h5 id="alert-message"></h5>
				</div>
			
			</div>
		</div>
	</div>
	



<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/bootstrap/js/popper.js"></script>
	<script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/daterangepicker/moment.min.js"></script>
	<script type="text/javascript" src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/slick/slick.min.js"></script>
	<script type="text/javascript" src="js/slick-custom.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/parallax100/parallax100.js"></script>
	<script type="text/javascript">
        $('.parallax100').parallax100();
	</script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/lightbox2/js/lightbox.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>
	<script src="js/custom.js"></script>


	<script>
		$(document).ready(function(){
			$("#newsletter").on("click", function(e){
				e.preventDefault();
				const email = $("#email-address").val();
				let message = "";
				if(validateEmail(email)){
					$.ajax({
						url: "./index.php",
						type: "POST",
						data: {
							action: "newsletter",
							email : email,
							
						},
						success: function(data){
							const jsonData = JSON.parse(data);
							if(jsonData['code'] === 200){
								
								message = "Successfully added to news letter";
								
							}else if(jsonData['code'] === 409){
								
								message = "Email already added to new letter";
							}else{
								message = "We are not able to add you in news letter right now, please try some time later";
							}
							if(message !== ""){
								showAlert(message);
							}
							
						}
					});
				}else{
					message = "Please enter a valid email";
				}
				if(message !== ""){
					showAlert(message);
				}

				$("#email-address").val("");
				return false;
				

			});

			$("#make-reservation").on("click", function(e){
				e.preventDefault();

				const date = $("[name = date]").val();
				const time = $("[name = time]").val();
				const people = parseInt($("[name = people]").val());
				const name = $("[name = name]").val();
				const phone = $("[name = phone]").val();
				const email = $("[name = email]").val();
				let isFormValid = true;
				
				if(date === ""){
					$("[name = date]").css({
						"background-color": "#ffe6e6",
					});
					isFormValid = false;
				}

				if(time === ""){
					$("[name = time]").css({
						"background-color": "#ffe6e6",
					});
					isFormValid = false;
				}


				if(people <= 0 || people === ""){
					$("[name = people]").css({
						"background-color": "#ffe6e6",
					});
					isFormValid = false;
				}

				if(name === ""){
					$("[name = name]").css({
						"background-color": "#ffe6e6",
					});
					isFormValid = false;
				}

				if(phone === ""){
					$("[name = phone]").css({
						"background-color": "#ffe6e6",
					});
					isFormValid = false;
				}

				if(email === ""){
					
					$("[name = email]").css({
						"background-color": "#ffe6e6",
					});
					isFormValid = false;
				}else{
					if(!validateEmail(email)){
						$("[name = email]").css({
							"background-color": "#ffe6e6",
						});
						isFormValid = false;
					}
				}


				if(isFormValid){
					$.ajax({
					url: "./index.php",
					type: "POST",
					data: {
						action: "reservation",
						date: date,
						time: time,
						people: people,
						name: name,
						phone: phone,
						email: email
					},
					success: function(data){
						const jsonData = JSON.parse(data);
						
						if(jsonData['code'] === 200){
							alert("Reservation booked");
						}else{
							alert("We are not able to reserve a table right now, please try some time later");
						}
					}
				})
				}
				return false;
			});

			const eventCount = parseInt("<?php echo count($eventsData); ?>");
			
			if(eventCount > 0){
				for(let i=1; i<=eventCount; i++){
					var countDownDate = $("#end_date" + i).val();
					

						// Update the count down every 1 second
						var x = setInterval(function() {

						// Get today's date and time
						var now = new Date().getTime();
						var eventDate = new Date(countDownDate).getTime();
						
							
						// Find the distance between now and the count down date
						var distance = eventDate - now;
						
							
						// Time calculations for days, hours, minutes and seconds
						var days = Math.floor(distance / (1000 * 60 * 60 * 24));
						var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
						var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));

						var seconds = Math.floor((distance % (1000 * 60)) / 1000);
							
						// Output the result in an element with id="demo"
						document.getElementById("days" + i).innerHTML = days;
						document.getElementById("hours" + i).innerHTML = hours;
						document.getElementById("minutes" + i).innerHTML = minutes;
						document.getElementById("seconds" + i).innerHTML = seconds;
							
						// If the count down is over, write some text 
						if (distance < 0) {
							clearInterval(x);
							document.getElementById("days" + i).innerHTML = "EXPIRED";
							$("#event-" + i).css("display", "none");
						}
					}, 1000);
				}
			}
		});

		function validateEmail(email) {
			const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(String(email).toLowerCase());
		}
		

		function addToCart(itemId){
			const cookie = getCookie();
			let itemIds = {};
			
			if(cookie){
				itemIds = JSON.parse(cookie);
				if(itemIds[itemId]){
					itemIds[itemId]++;
				}else{
					itemIds[itemId] = 1;
				}
			}else{
				itemIds[itemId] = 1;
			}

			
			
			let date = new Date();
			date.setTime(date.getTime() + (24 * 60 * 60 * 1000));
			const expires = "expires=" + date.toUTCString();
			document.cookie = "itemIds=" + JSON.stringify(itemIds) + "; " + expires + "; path=/";

			$("#item-in-cart").css("opacity", "1");

			$("#item-added-" + itemId).css("opacity", "1");

			setTimeout(() => {
				$("#item-added-" + itemId).css("opacity", "0");
			}, 1000);

		}


		function showModal(e, item){
			e.preventDefault();
			$("#item-img").attr("src", item['images']);
			$("#item-name").text(item['name']);
			$("#item-desc").text(item['description']);
			$("#item-price").text(parseFloat(item['price']).toFixed(0) + " PKR");
			let tags = "";
			if(parseInt(item['special_item']) === parseInt(1)){
				tags += "<span class='badge badge-danger mr-1'>Special item</span>";
			}

			if(parseInt(item['sold_out']) === parseInt(1)){
				tags += "<span class='badge badge-danger mr-1'>Sold out</span>";
			}

			if(parseInt(item['top_pick_today']) === parseInt(1)){
				tags += "<span class='badge badge-danger mr-1'>Top pick of the day</span>";
			}
			
			$("#item-tags").html(tags);
			$("#itemModal").modal('toggle');
		}



		function showAlert(message){
			$("#alert-message").text(message);
			$("#alertModal").modal('toggle');
		}
		
		
	</script>

</body>
</html>
